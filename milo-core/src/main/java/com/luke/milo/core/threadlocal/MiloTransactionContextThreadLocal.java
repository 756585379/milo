package com.luke.milo.core.threadlocal;

import com.luke.milo.common.context.MiloTransactionContext;

/**
 * @Descrtption 存储事务上线文在线程本地（和线程绑定）
 * @Author luke
 * @Date 2019/9/18
 **/
public class MiloTransactionContextThreadLocal {

    /**
     * thread local
     */
    private static final ThreadLocal<MiloTransactionContext> THREAD_LOCAL = new ThreadLocal<>();

    private static final MiloTransactionContextThreadLocal TRANSACTION_CONTEXT_THREAD_LOCAL = new MiloTransactionContextThreadLocal();

    private MiloTransactionContextThreadLocal() {

    }

    public static MiloTransactionContextThreadLocal getInstance() {
        return TRANSACTION_CONTEXT_THREAD_LOCAL;
    }

    public void setContext(final MiloTransactionContext context){
        THREAD_LOCAL.set(context);
    }

    public MiloTransactionContext getContext(){
        return THREAD_LOCAL.get();
    }

    public void removeContext(){
        THREAD_LOCAL.remove();
    }

}
